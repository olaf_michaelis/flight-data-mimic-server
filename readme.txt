This is the Flight Data Mimic Server.
It helps to find airport codes and dynamically emits flight data that can be displayed on the moving map.

Please visit https://airport-query.cloud10.net/.

If you need further help you will find it there.
